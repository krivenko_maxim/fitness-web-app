package fitness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.UserService;

@SpringComponent
public class UsersInit implements CommandLineRunner {
  private final UserService userService;


  @Autowired
  public UsersInit(UserService userService) {
    this.userService = userService;
  }



  @Override
  public void run(String... args) throws Exception {
    /*
     * User userForDevelopment = new User(); userForDevelopment.setEmail("dev@dev.com");
     * userForDevelopment.setPassword("12345678");
     * userForDevelopment.setFio("Unreal for development"); userForDevelopment.setRole("ROLE_USER");
     * userService.update(userForDevelopment);
     */
  }

}
