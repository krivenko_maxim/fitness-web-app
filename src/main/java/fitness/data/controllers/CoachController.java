package fitness.data.controllers;


import fitness.data.entity.Coach;
import fitness.data.services.CoachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/coaches")
public class CoachController {
    private final CoachService service;

    @Autowired
    public CoachController(CoachService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<Coach>> findAll() {
        return ResponseEntity.ok(service.getCoaches());
    }

    @GetMapping("ids/{id}")
    public ResponseEntity<Coach> findById(@PathVariable int id) {
        return ResponseEntity.ok(service.getById(id));
    }

}
