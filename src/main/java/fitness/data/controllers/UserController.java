package fitness.data.controllers;

import fitness.data.entity.User;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import fitness.data.services.UserService;

@RestController
@RequestMapping("api/users")
public class UserController {

  private final UserService service;

  @Autowired
  public UserController(UserService service) {
    this.service = service;
  }

  @GetMapping
  public ResponseEntity<List<User>> findAll() {
    return ResponseEntity.ok(service.findAll());
  }

  @GetMapping("ids/{id}")
  public ResponseEntity<User> findById(@PathVariable int id) {
    return ResponseEntity.ok(service.findById(id));
  }

  @GetMapping("emails/{email}")
  public ResponseEntity<User> findByEmail(@PathVariable String email) {
    return ResponseEntity.ok(service.findByEmail(email));
  }

  @PostMapping("authorize")
  public ResponseEntity<Object> authorize(@RequestParam String email, @RequestParam String password,
      HttpServletResponse response) {
    Cookie cookie = service.authorize(email, password);
    response.addCookie(cookie);
    return ResponseEntity.ok().build();
  }

  @PostMapping("register")
  public ResponseEntity<Object> register(@RequestBody User user) {
    service.register(user);
    return ResponseEntity.ok().build();
  }

  @PutMapping
  public ResponseEntity<Object> update(@RequestBody User user) {
    service.update(user);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping
  public ResponseEntity<Object> deleteAll() {
    service.deleteAll();
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("ids/{id}")
  public ResponseEntity<Object> deleteById(@PathVariable int id) {
    service.deleteById(id);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("emails/{email}")
  public ResponseEntity<Object> deleteByEmail(@PathVariable String email) {
    service.deleteByEmail(email);
    return ResponseEntity.ok().build();
  }
}
