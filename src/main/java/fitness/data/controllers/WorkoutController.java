package fitness.data.controllers;

import fitness.data.entity.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import fitness.data.services.WorkoutService;

import java.util.List;

@RestController
@RequestMapping("api/coaching")
public class WorkoutController {

    private final WorkoutService service;

    @Autowired
    public WorkoutController(WorkoutService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<Workout>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PutMapping
    public ResponseEntity<Object> update(@RequestBody Workout Workout) {
        service.update(Workout);
        return ResponseEntity.ok().build();
    }
}
