package fitness.data.controllers.handlers;

import fitness.data.exceptions.CoachNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CoachExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CoachNotFoundException.class)
    public ResponseEntity<Object> handleCoachNotFound(CoachNotFoundException e) {
        return ResponseEntity.notFound().build();
    }
}
