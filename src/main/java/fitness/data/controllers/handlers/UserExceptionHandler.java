package fitness.data.controllers.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import fitness.data.exceptions.EmailAlreadyUsedException;
import fitness.data.exceptions.InvalidCredentialsException;
import fitness.data.exceptions.UserNotFoundException;

@RestControllerAdvice
public class UserExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<Object> handleUserNotFound(UserNotFoundException e) {
    return ResponseEntity.notFound().build();
  }

  @ExceptionHandler(EmailAlreadyUsedException.class)
  public ResponseEntity<Object> handleEmailAlreadyUsed(EmailAlreadyUsedException e) {
    return ResponseEntity.status(HttpStatus.CONFLICT).build();
  }

  @ExceptionHandler(InvalidCredentialsException.class)
  public ResponseEntity<Object> handleInvalidCredentials(InvalidCredentialsException e) {
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }
}
