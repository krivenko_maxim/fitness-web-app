package fitness.data.entity;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name ="category")
public class Category {

    @Id
    @Column(name = "category_name", nullable = false, length = 50)
    private String name;

    @ManyToMany(mappedBy = "categories")
    private List<User> users = new LinkedList<>();

    @OneToMany(mappedBy = "category")
    private List<Video> videos = new LinkedList<>();
}
