package fitness.data.entity;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "coach")
public class Coach {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "main_info")
    private String mainInfo;

    @Column(name = "education")
    private String education;

    @Column(name = "work_history")
    private String workHistory;

    @Column(name = "about_me")
    private String aboutMe;

    @Column(name = "main_photo")
    private byte[] mainPhoto;

    @Column(name = "second_photo")
    private byte[] secondPhoto;

    @Column(name = "preview_photo")
    private byte[] previewPhoto;

    @Column(name = "preview_main_text")
    private String previewMainText;

    @Column(name = "preview_small_text")
    private String previewSmallText;

    @OneToMany(mappedBy = "coach", fetch = FetchType.EAGER)
    private List<Specialization> specializations = new LinkedList<>();
}