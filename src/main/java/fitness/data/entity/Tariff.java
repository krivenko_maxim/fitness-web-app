package fitness.data.entity;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "tariff")
public class Tariff {

    @Id
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "tariff")
    private List<User> users = new LinkedList<>();

    @OneToMany(mappedBy = "tariff")
    private List<TemplateForNutrition> templateForNutritions = new LinkedList<>();

    @OneToMany(mappedBy = "tariff", fetch = FetchType.LAZY)
    private List<TariffDescription> tariffDescriptions = new LinkedList<>();

}
