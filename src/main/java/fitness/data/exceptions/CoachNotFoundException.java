package fitness.data.exceptions;

public class CoachNotFoundException extends InvalidCredentialsException {

    public CoachNotFoundException() {
        super();
    }

    public CoachNotFoundException(String message) {
        super(message);
    }

    public CoachNotFoundException(Throwable cause) {
        super(cause);
    }

    public CoachNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
