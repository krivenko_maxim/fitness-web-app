package fitness.data.exceptions;

public class InvalidCredentialsException extends RuntimeException {

  public InvalidCredentialsException() {
    super();
  }

  public InvalidCredentialsException(String message) {
    super(message);
  }

  public InvalidCredentialsException(Throwable cause) {
    super(cause);
  }

  public InvalidCredentialsException(String message, Throwable cause) {
    super(message, cause);
  }
}
