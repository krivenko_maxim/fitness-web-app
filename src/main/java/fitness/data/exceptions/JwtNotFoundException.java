package fitness.data.exceptions;

public class JwtNotFoundException extends RuntimeException {

  public JwtNotFoundException() {
    super();
  }

  public JwtNotFoundException(String message) {
    super(message);
  }

  public JwtNotFoundException(Throwable cause) {
    super(cause);
  }

  public JwtNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
