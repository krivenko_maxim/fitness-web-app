package fitness.data.repositories;

import fitness.data.entity.Category;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("select c from Category c where c.name = ?1")
    Optional<Category> getCategoryByName(String workoutName);

    @Query("select c from Category c inner join c.videos videos where c.name = ?1 and videos.id = ?2")
    Optional<Category> findByWorkoutNameEqualsAndVideos_Id(String workoutName, Integer id);


}
