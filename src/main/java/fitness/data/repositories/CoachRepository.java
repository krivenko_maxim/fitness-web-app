package fitness.data.repositories;

import fitness.data.entity.Coach;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoachRepository extends JpaRepository<Coach, Integer> {

    Optional<Coach> findCoachById(Integer id);

}
