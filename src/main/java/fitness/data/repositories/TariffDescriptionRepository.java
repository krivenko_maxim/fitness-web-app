package fitness.data.repositories;

import fitness.data.entity.TariffDescription;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TariffDescriptionRepository extends JpaRepository<TariffDescription, Integer> {

    @Query("select t from TariffDescription t where t.tariff.name = ?1 order by t.id")
    List<TariffDescription> findByTariffName(String name);

}
