package fitness.data.repositories;

import fitness.data.entity.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TariffRepository extends JpaRepository<Tariff, String> {
    @Transactional
    Optional<Tariff> findByName(String name);
}
