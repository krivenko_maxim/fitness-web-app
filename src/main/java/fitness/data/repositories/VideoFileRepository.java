package fitness.data.repositories;

import com.vaadin.flow.server.StreamResource;
import fitness.data.exceptions.VideoNotFoundException;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.UUID;

@Component
public class VideoFileRepository
{
  private static final String CLASSPATH_TO_VIDEOS = "src/main/resources/META-INF/resources/videos/";

  public StreamResource getStreamResourceOfVideo(String path) {
    return new StreamResource(UUID.randomUUID().toString(), () -> {
      try {
        return new FileInputStream(CLASSPATH_TO_VIDEOS + path);
      }
      catch (FileNotFoundException e) {
        throw new VideoNotFoundException(e);
      }
    });
  }
}
