package fitness.data.repositories;

import fitness.data.entity.User;
import fitness.data.entity.Weight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WeightRepository extends JpaRepository<Weight, Integer> {

    List<Weight> getAllByUser(User user);

    Integer countAllByUser(User user);
}
