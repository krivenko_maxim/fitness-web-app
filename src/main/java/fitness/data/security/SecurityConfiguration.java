package fitness.data.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import fitness.data.security.jwt.JwtFilter;
import fitness.data.services.JwtUserService;

// these two slashes, commented annotation above JwtFilter
// and sending cookie back to browser in authorization view
// are the only things that keep security off
// @EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final JwtUserService jwtUserService;
  private final JwtFilter jwtFilter;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public SecurityConfiguration(JwtUserService jwtUserService, JwtFilter jwtFilter,
      PasswordEncoder passwordEncoder) {
    this.jwtUserService = jwtUserService;
    this.jwtFilter = jwtFilter;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(daoAuthenticationProvider());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().formLogin().disable().httpBasic().disable().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().requestCache()
        .requestCache(new CustomRequestCache()).and().authorizeRequests()
        .requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll()
        .antMatchers("/authorization", "/registration").permitAll().antMatchers("**")
        .hasRole("USER").and().addFilterAfter(jwtFilter, BasicAuthenticationFilter.class)
        .exceptionHandling().authenticationEntryPoint(new UnauthorizedEntryPoint()).and()
        .exceptionHandling().accessDeniedPage("/authorization");

  }

  /**
   * Allows access to static resources, bypassing Spring security.
   */
  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(
        // Vaadin Flow static resources
        "/VAADIN/**",

        // the standard favicon URI
        "/favicon.ico",

        // the robots exclusion standard
        "/robots.txt",

        // (development mode) static resources
        "/frontend/**",

        // (production mode) static resources
        "/frontend-es5/**", "/frontend-es6/**");
  }

  @Bean
  public DaoAuthenticationProvider daoAuthenticationProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setPasswordEncoder(passwordEncoder);
    provider.setUserDetailsService(jwtUserService);
    return provider;
  }
}
