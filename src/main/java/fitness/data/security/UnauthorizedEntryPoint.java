package fitness.data.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {

  private static final String URL_TO_REDIRECT = "/authorization";
  private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException authException) throws IOException, ServletException {
    if (response.isCommitted()) {
      return;
    }
    redirectStrategy.sendRedirect(request, response, URL_TO_REDIRECT);
  }
}
