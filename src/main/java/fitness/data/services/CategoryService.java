package fitness.data.services;

import fitness.data.entity.Category;
import fitness.data.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(@Autowired CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category getCategoryByName(String name) {
        return categoryRepository.getCategoryByName(name).orElseGet(() -> {
            Category newCategory = new Category();
            newCategory.setName(name);
            categoryRepository.save(newCategory);
            return newCategory;
        });
    }

    public void saveCategory(Category category) {
        categoryRepository.save(category);
    }

}
