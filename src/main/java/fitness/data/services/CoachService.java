package fitness.data.services;

import fitness.data.entity.Coach;
import fitness.data.exceptions.CoachNotFoundException;
import fitness.data.repositories.CoachRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CoachService extends CrudService<Coach, Integer> {

    private final CoachRepository repository;

    @Getter
    private List<Coach> cachedCoaches;

    public CoachService(@Autowired CoachRepository repository) {
        this.repository = repository;
        updateCoachesParallel();
    }

    public void updateCoaches() {
        cachedCoaches = repository.findAll();
    }

    public void updateCoachesParallel() {
        Thread thread = new Thread(() -> {
            if (cachedCoaches == null) {
                cachedCoaches = new ArrayList<>();
            }
            this.updateCoaches();
        });
        thread.start();
    }

    public Coach getById(Integer id) {
        Optional<Coach> coachOptional =
            cachedCoaches.stream().filter(coach -> Objects.equals(coach.getId(), id)).findFirst();
        if (coachOptional.isEmpty()) {
            return repository.findCoachById(id).orElseThrow(
                    () -> new CoachNotFoundException(String.format("Coach with id %d not found", id)));
        }
        return coachOptional.get();
    }

    public List<Coach> getCoaches() {
        return this.cachedCoaches;
    }

    @Override
    public Coach update(Coach entity) {
        return super.update(entity);
    }

    @Override
    protected CoachRepository getRepository() {
        return repository;
    }
}
