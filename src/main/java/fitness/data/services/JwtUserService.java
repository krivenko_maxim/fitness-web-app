package fitness.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import fitness.data.repositories.UserRepository;

// We use emails instead of usernames to uniquely identify who is trying to authorize
// but spring's built-in UserDetailsService has username in method's name.
// Jwts need service that implements UserDetailsService
// so this service is for spring's internal use only.
// For external use refer to JwtUserServiceAdapter instead.
@Service
public class JwtUserService implements UserDetailsService {

  private final UserRepository userRepository;

  @Autowired
  public JwtUserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return userRepository.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException(String.format("%s not found", username)));
  }

}
