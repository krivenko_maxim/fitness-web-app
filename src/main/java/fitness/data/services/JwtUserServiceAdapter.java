package fitness.data.services;

import fitness.data.exceptions.EmailNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserServiceAdapter {

  private final JwtUserService jwtUserService;

  @Autowired
  public JwtUserServiceAdapter(JwtUserService jwtUserService) {
    this.jwtUserService = jwtUserService;
  }

  public UserDetails loadUserByEmail(String email) {
    try {
      return jwtUserService.loadUserByUsername(email);
    } catch (UsernameNotFoundException e) {
      throw new EmailNotFoundException(e.getMessage(), e);
    }
  }
}
