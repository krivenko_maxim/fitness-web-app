package fitness.data.services;

import fitness.data.entity.TariffDescription;
import fitness.data.repositories.TariffDescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.List;
import java.util.Optional;

@Service
public class TariffDescriptionService extends CrudService<TariffDescription, Integer> {

    private final TariffDescriptionRepository repository;

    public TariffDescriptionService(@Autowired TariffDescriptionRepository repository) {
        this.repository = repository;
    }

    @Override
    public TariffDescription update(TariffDescription description) {
        return super.update(description);
    }

    public List<TariffDescription> getDescriptionsByTariffName(String name) {
        return repository.findByTariffName(name);
    }

    @Override
    public Optional<TariffDescription> get(Integer id) {
        return super.get(id);
    }

    public List<TariffDescription> findAll() {
        return repository.findAll();
    }

    @Override
    protected JpaRepository<TariffDescription, Integer> getRepository() {
        return repository;
    }
}
