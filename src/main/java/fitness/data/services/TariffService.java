package fitness.data.services;

import com.vaadin.flow.router.NotFoundException;
import fitness.data.entity.Tariff;
import fitness.data.entity.TariffDescription;
import fitness.data.repositories.TariffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class TariffService extends CrudService<Tariff, String> {

    private final TariffRepository repository;

    public TariffService(@Autowired TariffRepository repository) {
        this.repository = repository;
    }

    public Optional<Tariff> getTariffByName(String tariffName) {
        return this.getRepository().findByName(tariffName);
    }

    public List<String> getTariffInfo(String tariffName) {
        List<TariffDescription> tariffDescriptions = getTariffByName(tariffName).orElseThrow(NotFoundException::new).getTariffDescriptions();
        List<String> descriptions = new ArrayList<>();
        for (TariffDescription description : tariffDescriptions) {
            descriptions.add(description.getInfo());
        }
        return descriptions;
    }

    public List<Tariff> findAll() {
        return repository.findAll();
    }

    @Override
    protected TariffRepository getRepository() {
        return repository;
    }

    @Override
    public Tariff update(Tariff entity) {
        return super.update(entity);
    }
}
