package fitness.data.services;

import fitness.data.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.Cookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;
import fitness.data.exceptions.EmailAlreadyUsedException;
import fitness.data.exceptions.InvalidCredentialsException;
import fitness.data.exceptions.UserNotFoundException;
import fitness.data.repositories.UserRepository;
import fitness.data.security.jwt.JwtUtils;

@Service
public class UserService extends CrudService<User, Integer> {

  private final UserRepository repository;
  private final PasswordEncoder passwordEncoder;
  private final JwtUtils jwtUtils;

  private byte[] defaultUserPhoto;

  @Autowired
  public UserService(UserRepository repository, PasswordEncoder passwordEncoder, JwtUtils jwtUtils)
      throws IOException {
    this.repository = repository;
    this.passwordEncoder = passwordEncoder;
    this.jwtUtils = jwtUtils;
    ClassPathResource defaultUserPhotoResource =
        new ClassPathResource("\\META-INF\\resources\\images\\profile\\default-user-photo.png");
    defaultUserPhoto = defaultUserPhotoResource.getInputStream().readAllBytes();
  }

  public List<User> findAll() {
    return repository.findAll();
  }

  public User findById(int id) {
    return repository.findById(id).orElseThrow(
        () -> new UserNotFoundException(String.format("User with id %d not found", id)));
  }

  public User findByEmail(String email) {
    return repository.findByEmail(email).orElseThrow(
        () -> new UserNotFoundException(String.format("User with email %s not found", email)));
  }

  public User getUserFromCookies(Cookie[] cookies) {
    String email = jwtUtils.getSubjectFromJwt(jwtUtils.getJwtFromCookies(cookies));
    return findByEmail(email);
  }

  public boolean emailAlreadyUsed(String email) {
    return !repository.findByEmail(email).isEmpty();
  }

  /**
   * Try to authorize with given email and password
   * 
   * @param email email to use for authorization
   * @param password password to use for authorization
   * 
   * @return cookie with jwt to hand over to client
   * 
   * @throws UserNotFoundException if email isn't present in database
   * @throws InvalidCredentialsException if passwords don`t match
   */
  public Cookie authorize(String email, String password) {
    User user = findByEmail(email);
    if (!passwordEncoder.matches(password, user.getPassword())) {
      throw new InvalidCredentialsException("Invalid password");
    }
    return jwtUtils.generateCookieWithToken(email);
  }

  public User register(User user) {
    if (emailAlreadyUsed(user.getEmail())) {
      throw new EmailAlreadyUsedException(
          String.format("Email %s is already used", user.getEmail()));
    }
    User userToRegister = new User();
    userToRegister.setFirstName(user.getFirstName());
    userToRegister.setLastName(user.getLastName());
    userToRegister.setEmail(user.getEmail());
    userToRegister.setPassword(passwordEncoder.encode(user.getPassword()));
    userToRegister.setRoles("ROLE_USER");
    userToRegister.setPhoto(defaultUserPhoto);
    return repository.save(userToRegister);
  }

  public User update(User user) {
    if (!repository.existsById(user.getId())) {
      throw new UserNotFoundException(String.format("User with id %d is not found", user.getId()));
    }
    return repository.save(user);
  }

  public User updatePersonalData(User user) {
    User existingUser =
        repository.findById(user.getId()).orElseThrow(() -> new UserNotFoundException(
            String.format("User with id %d is not found", user.getId())));
    existingUser.setFirstName(user.getFirstName());
    existingUser.setLastName(user.getLastName());
    if (!user.getPassword().equals(existingUser.getPassword())) {
      existingUser.setPassword(passwordEncoder.encode(user.getPassword()));
    }
    existingUser.setEmail(user.getEmail());
    existingUser.setPhoto(user.getPhoto());
    return repository.save(existingUser);
  }

  public void deleteAll() {
    repository.deleteAll();
  }

  public void deleteById(int id) {
    repository.deleteById(id);
  }

  public void deleteByEmail(String email) {
    repository.deleteByEmail(email);
  }

  @Override
  protected UserRepository getRepository() {
    return repository;
  }
}
