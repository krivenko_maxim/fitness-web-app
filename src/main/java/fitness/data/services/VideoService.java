package fitness.data.services;

import com.vaadin.flow.server.StreamResource;
import fitness.data.entity.User;
import fitness.data.entity.Video;
import fitness.data.exceptions.VideoNotFoundException;
import fitness.data.repositories.VideoFileRepository;
import fitness.data.repositories.VideoRepository;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.List;

@Service
public class VideoService extends CrudService<Video, Integer> {

    private final VideoRepository videoRepository;
    private final VideoFileRepository videoFileRepository;

    public VideoService(@Autowired VideoRepository videoRepository,
                        @Autowired VideoFileRepository videoFileRepository){
        this.videoFileRepository = videoFileRepository;
        this.videoRepository = videoRepository;
    }

    public List<Video> getVideos() {
        return this.getRepository().findAll();
    }

    public void addVideo(Video video)
    {
        this.videoRepository.save(video);
    }

    protected VideoRepository getRepository() {
        return videoRepository;
    }

    public Video findById(Integer id) {
        return videoRepository.findById(id).orElseThrow(
                () -> new VideoNotFoundException(String.format("Video with id %d not found", id)));
    }

    public List<Video> findByWorkoutName(String name) {
        return videoRepository.findByWorkoutName(name);
    }

    public Video findByWorkoutNameAndWorkoutId(String name, Integer id) {
        return videoRepository.findByWorkoutNameAndWorkoutId(name, id).orElseThrow(
                () -> new VideoNotFoundException(String.format("Video with workout name %s and id %d not found",
                        name, id)));
    }

    public List<Video> getVideosByUser(User user) {

        return videoRepository.findVideosOfUser(user);
    }

    public StreamResource getStreamResourceOfVideo(String path)
    {
        return videoFileRepository.getStreamResourceOfVideo(path);
    }
}
