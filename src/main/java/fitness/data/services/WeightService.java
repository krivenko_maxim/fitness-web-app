package fitness.data.services;

import fitness.data.entity.Weight;
import fitness.data.repositories.UserRepository;
import fitness.data.repositories.WeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeightService extends CrudService<Weight, Integer> {

    private final WeightRepository weightRepository;
    private final UserRepository userRepository;

    public WeightService(@Autowired WeightRepository weightRepository,
                         @Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
        this.weightRepository = weightRepository;
    }

    public List<Weight> getUsers() {
        return this.getRepository().findAll();
    }

    @Override
    protected WeightRepository getRepository() {
        return weightRepository;
    }

    public List<Weight> getWeightByUserId(Integer id)
    {
        var user = userRepository.findById(id);
        if (user.isPresent() && weightRepository.countAllByUser(user.get()) > 0)
        {
            return weightRepository.getAllByUser(user.get());
        }
        return new ArrayList<>();
    }
}
