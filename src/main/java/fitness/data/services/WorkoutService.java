package fitness.data.services;

import fitness.data.entity.Workout;
import fitness.data.repositories.WorkoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.util.List;

@Service
public class WorkoutService extends CrudService<Workout, Integer> {

    private final WorkoutRepository repository;

    public WorkoutService(@Autowired WorkoutRepository repository) {
        this.repository = repository;

    }
    public List<Workout> findAll(){
        return repository.findAll();
    }
    @Override
    public Workout update(Workout entity) {
        return super.update(entity);
    }

    @Override
    protected JpaRepository<Workout, Integer> getRepository() {
        return repository;
    }
}
