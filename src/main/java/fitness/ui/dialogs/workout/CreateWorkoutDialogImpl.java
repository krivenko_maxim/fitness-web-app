package fitness.ui.dialogs.workout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import fitness.data.entity.Tariff;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@StyleSheet("./fonts.css")
@StyleSheet("./workouts-styles/workout.css")
public class CreateWorkoutDialogImpl extends Dialog implements CreateWorkoutDialog {

    private static final int LITE_SIZE = 3;
    private static final int ZERO_SIZE = 0;
    private static final int MIN_ACCEPTABLE_BASE_SIZE = 7;

    private final CreateWorkoutPresenter presenter;
    private final List<String> baseWorkoutsList = new ArrayList<>();
    private final List<UnorderedList> baseUnorderedLists = List.of(new UnorderedList(), new UnorderedList(),
        new UnorderedList());
    private final List<String> workoutList = List.of("Программа 1", "Программа 2", "Программа 3", "Программа 4",
        "Программа 5", "Программа 6");

    private Button buyButton;
    private H4 numberOfWorkoutsText;
    private Collection<String> listOfChosenWorkouts;
    private UnorderedList singleUnorderedList = new UnorderedList();


    public CreateWorkoutDialogImpl(@Autowired CreateWorkoutPresenter presenter) {
        this.presenter = presenter;
        init();
    }

    @Override
    public void init() {
        singleUnorderedList.getClassNames().add("unordered-list");
        baseUnorderedLists.forEach(item -> item.getClassNames().add("unordered-list"));

        H2 headline = new H2("Ваш заказ:");
        headline.getStyle().set("margin-right", "auto").set("font-size", "1.5em")
            .set("font-weight", "bold").set("font-family", "Montserrat, sans-serif");

        VerticalLayout layout = new VerticalLayout();
        layout.add(headline);
        layout.setClassName("center-align");
        layout.setWidth("480px");

        Tariff tariff = presenter.getTariff();
        buyButton = new Button("Оплатить",
            buttonClickEvent -> presenter.handleCreateClick(listOfChosenWorkouts, presenter.getTariff()));
        buyButton.setClassName("buy");
        buyButton.setEnabled(false);
        buyButton.setDisableOnClick(true);

        switch (tariff.getName()) {
            case ("Lite") -> {
                setupPriceLayout(ZERO_SIZE, layout);
                layout.add(createLayoutOfWorkouts(singleUnorderedList));
            }
            case ("Base") -> {
                setupPriceLayout(baseWorkoutsList.size(), layout);
                Div div = new Div();
                baseUnorderedLists.forEach((item) -> div.add(createLayoutOfWorkouts(item)));
                layout.add(div);
            }
            case ("Maximum") -> {
                layout.add(createListForMaximumLayout(layout));
                buyButton.setEnabled(true);
            }
        }

        H4 price = new H4("Сумма: " + presenter.getTariff().getPrice() + " руб.");
        price.getStyle().set("margin-left", "auto");
        layout.add(price, buyButton);
        add(layout);
    }

    private UnorderedList createListForMaximumLayout(VerticalLayout verticalLayout) {
        Collection<String> descriptions = presenter.getAllDescriptions();
        setupPriceLayout(descriptions.size(), verticalLayout);
        singleUnorderedList = new UnorderedList();

        for (String s : descriptions) {
            ListItem item = new ListItem(s);
            item.setClassName("marker");
            singleUnorderedList.add(item);
        }
        listOfChosenWorkouts = descriptions;
        return singleUnorderedList;
    }

    private VerticalLayout createLayoutOfWorkouts(UnorderedList unorderedList) {
        Select<String> select = new Select<>();
        select.setItems(workoutList);
        VerticalLayout layout = new VerticalLayout();
        layout.add(select);
        layout.getClassNames().add("center-align");
        layout.getStyle().remove("width");
        setupWorkoutDescription(unorderedList, layout, select);
        return layout;
    }

    private void setupPriceLayout(int size, VerticalLayout layout) {
        numberOfWorkoutsText = new H4("Количество тренировок:  " + size);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(numberOfWorkoutsText);
        horizontalLayout.getStyle().set("margin", "0");
        layout.add(new Hr(), horizontalLayout, new Hr());
    }

    private void setupWorkoutDescription(@NotNull UnorderedList unorderedList, VerticalLayout layout,
        Select<String> select) {
        select.addValueChangeListener(changeEvent -> {
            layout.getChildren().forEach((item) -> {
                if (item == unorderedList) {
                    layout.remove(unorderedList);
                }
            });
            String selectedWorkout = changeEvent.getValue();
            unorderedList.removeAll();

            for (String s : presenter.getWorkoutDescriptionByProgram(selectedWorkout)) {
                ListItem item = new ListItem(s);
                item.setClassName("marker");
                unorderedList.add(item);
                if (unorderedList != singleUnorderedList) {
                    baseWorkoutsList.add(s);
                }
            }

            if (unorderedList != singleUnorderedList) {
                List<String> listOfPreviousWorkouts = presenter.getWorkoutDescriptionByProgram(
                    changeEvent.getOldValue());
                if (listOfPreviousWorkouts != null) {
                    listOfPreviousWorkouts.forEach(baseWorkoutsList::remove);
                }

                Set<String> workouts = new HashSet<>(baseWorkoutsList);
                numberOfWorkoutsText.setText("Количество тренировок: " + workouts.size());
                if (workouts.size() >= MIN_ACCEPTABLE_BASE_SIZE) {
                    listOfChosenWorkouts = workouts;
                    buyButton.setEnabled(true);
                }
            } else {
                numberOfWorkoutsText.setText("Количество тренировок: " + LITE_SIZE);
                listOfChosenWorkouts = presenter.getWorkoutDescriptionByProgram(changeEvent.getValue());
                buyButton.setEnabled(true);
            }
            layout.add(unorderedList);
        });
    }

    @Override
    public void bind() {
    }
}
