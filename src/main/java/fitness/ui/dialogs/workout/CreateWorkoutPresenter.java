package fitness.ui.dialogs.workout;

import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Tariff;
import fitness.ui.mvp.Presenter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CreateWorkoutPresenter extends Presenter<CreateWorkoutDialog> {

    @Setter
    @Getter
    private Tariff tariff;

    @Setter
    private HashMap<String, List<String>> workouts;

    public void handleCreateClick(Collection<String> listOfChosenPrograms, Tariff name) {
        //TODO::заполнение тарифа и доступных видео для клиента,
        // если клиент не авторизован, то перекидывать на страницу авторизации
        view.close();
    }

    public List<String> getWorkoutDescriptionByProgram(String program) {
        return workouts.get(program);
    }

    public Collection<String> getAllDescriptions() {
        Set<String> list = new HashSet<>();
        workouts.values().forEach(list::addAll);
        return list;
    }
}
