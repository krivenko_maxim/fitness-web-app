package fitness.ui.vaadin.components;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import java.io.IOException;
import lombok.Getter;

public class CustomUpload extends Upload {

  private MemoryBuffer buffer;

  @Getter
  private byte[] uploadedFile;

  public CustomUpload() {
    init();
  }

  public CustomUpload(Label label) {
    init();
    setDropLabel(label);
  }

  public boolean isEmpty() {
    return uploadedFile == null;
  }

  private void init() {
    buffer = new MemoryBuffer();
    setReceiver(buffer);
    addFinishedListener(event -> {
      try {
        uploadedFile = buffer.getInputStream().readAllBytes();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    getElement().addEventListener("upload-abort", new DomEventListener() {
      @Override
      public void handleEvent(DomEvent event) {
        uploadedFile = null;
      }
    });

  }

}
