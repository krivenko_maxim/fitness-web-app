package fitness.ui.vaadin.components;

import com.vaadin.flow.component.upload.UploadI18N;

import java.util.Arrays;
  
public class CustomUploadI18N extends UploadI18N
{
  public CustomUploadI18N()
  {
    setDropFiles(new DropFiles()
            .setOne("Переместите файл сюда")
            .setMany("Переместите файлы сюда"));
    setAddFiles(new AddFiles()
            .setOne("Загрузить файл...")
            .setMany("Загрузить файлы..."));
    setCancel("Отмена");
    setError(new Error()
            .setTooManyFiles("Слишком много файлов.")
            .setFileIsTooBig("Файл слишком большой.")
            .setIncorrectFileType("Неверный тип файла."));
    setUploading(new Uploading()
            .setStatus(new Uploading.Status()
                    .setConnecting("Подключение...")
                    .setStalled("Застопорился")
                    .setProcessing("Обработка файла...")
                    .setHeld("В очереди"))
            .setRemainingTime(new Uploading.RemainingTime()
                    .setPrefix("оставшееся время: ")
                    .setUnknown("оставшееся время неизвестно"))
            .setError(new Uploading.Error()
                    .setServerUnavailable("Загрузка не удалась, попробуйте позже")
                    .setUnexpectedServerError("Загрузка не удалась вследствие ошибки сервера")
                    .setForbidden("Загрузка запрещена")));
    setUnits(new Units()
            .setSize(Arrays.asList("Б", "кБ", "МБ", "ГБ", "ТБ", "ПБ", "ЕБ", "ЗБ", "ЙБ")));
  }
}
