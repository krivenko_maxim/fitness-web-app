package fitness.ui.vaadin.components;

import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.receivers.AbstractFileBuffer;

import com.vaadin.flow.component.upload.receivers.UploadOutputStream;
import fitness.data.exceptions.VideoNotFoundException;
import fitness.ui.views.admin.videoupload.VideoUploadPresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class CustomVideoReceiver implements Receiver
{
  private static final String EXTENSION = ".mp4";
  private static final String PATH_TO_VIDEOS = "src/main/resources/META-INF/resources/videos/";
  private static final String PREFIX = "video_";

  private static final String defaultWorkoutName = "default_name";

  private final VideoUploadPresenter videoUploadPresenter;

  private String currentWorkoutName = defaultWorkoutName;

  public CustomVideoReceiver(@Autowired VideoUploadPresenter videoUploadPresenter)
  {
    this.videoUploadPresenter = videoUploadPresenter;
  }

  public void setWorkoutName(String workoutName)
  {
    currentWorkoutName = workoutName;
  }
  
  @Override
  public OutputStream receiveUpload(String filename, String mimetype)
  {
    long time = System.currentTimeMillis();
    String fileName = PREFIX + time + EXTENSION;
    try
    {
      File fileToBeCreated = Files.createFile(Path.of(PATH_TO_VIDEOS + fileName)).toFile();
      videoUploadPresenter.saveVideo(currentWorkoutName, fileName);
      return new UploadOutputStream(fileToBeCreated);
    }
    catch (IOException e)
    {
      throw new VideoNotFoundException(e);
    }
  }
}
