package fitness.ui.vaadin.components.canvas;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;

@Tag("canvas")
public class Canvas extends Component implements HasStyle, HasSize {

  private CanvasRenderingContext2D context;

  public Canvas(int width, int height) {
    context = new CanvasRenderingContext2D(this);

    getElement().setAttribute("width", String.valueOf(width));
    getElement().setAttribute("height", String.valueOf(height));
  }

  public CanvasRenderingContext2D getContext() {
    return context;
  }

  @Override
  public void setWidth(String width) {
    HasSize.super.setWidth(width);
  }

  @Override
  public void setHeight(String height) {
    HasSize.super.setHeight(height);
  }

  @Override
  public void setSizeFull() {
    HasSize.super.setSizeFull();
  }
}