package fitness.ui.vaadin.components.canvas;

import java.io.Serializable;

/**
 * The context for rendering shapes and images on a canvas.
 * <p>
 * This is a Java wrapper for the <a href=
 * "https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D">same
 * client-side API</a>.
 */
public class CanvasRenderingContext2D {

  private Canvas canvas;

  protected CanvasRenderingContext2D(Canvas canvas) {
    this.canvas = canvas;
  }

  protected void setProperty(String propertyName, Serializable value) {
    runScript(String.format("$0.getContext('2d').%s='%s'", propertyName,
            value));
  }

  private void runScript(String script) {
    canvas.getElement().getNode().runWhenAttached(
            ui -> ui.getInternals().getStateTree().beforeClientResponse(
                    canvas.getElement().getNode(),
                    context -> ui.getPage().executeJs(script, canvas.getElement())));
  }

  protected void callJsMethod(String methodName, Serializable... parameters) {
    canvas.getElement().callJsFunction("getContext('2d')." + methodName,
            parameters);
  }

}