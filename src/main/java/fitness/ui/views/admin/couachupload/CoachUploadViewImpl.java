package fitness.ui.views.admin.couachupload;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import fitness.data.entity.Coach;
import fitness.data.entity.Specialization;
import fitness.ui.vaadin.components.CustomUpload;
import fitness.ui.views.utils.Validator;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route("coach-upload")
public class CoachUploadViewImpl extends Div implements CoachUploadView {

  private final CoachUploadPresenter presenter;
  private Binder<Coach> binder;
  private final VerticalLayout inputLayout = new VerticalLayout();
  private TextField name;
  private TextField mainInfo;
  private TextField specializations;
  private TextField education;
  private TextField workHistory;
  private TextField aboutMe;
  private CustomUpload mainPhoto;
  private CustomUpload secondPhoto;
  private CustomUpload previewPhoto;
  private TextField previewMainText;
  private TextField previewSmallText;

  public CoachUploadViewImpl(@Autowired CoachUploadPresenter presenter) {
    this.binder = new Binder<>();
    this.presenter = presenter;
    this.presenter.onAttach(this);
  }

  @Override
  @PostConstruct
  public void init() {
    setupInputLayout();
    bind();
    add(inputLayout);
  }

  private void setupInputLayout() {
    name = new TextField("Имя");
    mainInfo = new TextField("Основная информация");
    specializations = new TextField("Специализации", "Стретчинг, Пилатес");
    education = new TextField("Образование");
    workHistory = new TextField("История работы");
    aboutMe = new TextField("Обо мне");
    previewMainText = new TextField("Основной текст превью");
    previewSmallText = new TextField("Короткий текст превью");
    mainPhoto = new CustomUpload(new Label("Главное фото"));
    secondPhoto = new CustomUpload(new Label("Второе фото"));
    previewPhoto = new CustomUpload(new Label("Превью фото"));

    inputLayout.add(name, mainInfo, specializations, education, workHistory, aboutMe,
        previewMainText, previewSmallText, mainPhoto, secondPhoto, previewPhoto);
    Button submitButton = new Button("Отправить", event -> {
      try {
        validateImages();
        binder.writeBean(presenter.getCoachEntity());
        presenter.getCoachEntity().setMainPhoto(mainPhoto.getUploadedFile());
        presenter.getCoachEntity().setSecondPhoto(secondPhoto.getUploadedFile());
        presenter.getCoachEntity().setPreviewPhoto(previewPhoto.getUploadedFile());
        presenter.update();
        showNotification("Успешно загружено", NotificationVariant.LUMO_SUCCESS);
      } catch (IllegalArgumentException e) {
        showNotification(e.getMessage(), NotificationVariant.LUMO_ERROR);
      } catch (ValidationException e) {
        e.printStackTrace();
      }
    });
    inputLayout.add(submitButton);
  }

  private void validateImages() {
    if (mainPhoto.isEmpty()) {
      throw new IllegalArgumentException("Пустое поле главного фото");
    }
    if (secondPhoto.isEmpty()) {
      throw new IllegalArgumentException("Пустое поле второго фото");
    }
    if (previewPhoto.isEmpty()) {
      throw new IllegalArgumentException("Пустое поле превью фото");
    }
  }

  private List<Specialization> parseSpecializations(String specializations) {
    return Arrays.stream(specializations.split(",")).map(string -> {
      Specialization specialization = new Specialization();
      specialization.setInfo(string.trim());
      specialization.setCoach(presenter.getCoachEntity());
      return specialization;
    }).toList();
  }

  @Override
  public void bind() {
    binder.setBean(presenter.getCoachEntity());
    binder.forField(name).withValidator(Validator.getEmptyStringValidator(), "Пустое поле имени")
        .bind(Coach::getName, Coach::setName);
    binder.forField(mainInfo)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле основной информации")
        .bind(Coach::getMainInfo, Coach::setMainInfo);
    binder.forField(specializations).withValidator(Validator.getEmptyStringValidator(), "")
        .withConverter(text -> parseSpecializations(text), list -> {
          if (list == null) {
            return "";
          }
          return list.stream().map(Specialization::getInfo).collect(Collectors.joining(","));
        }).bind(Coach::getSpecializations, Coach::setSpecializations);
    binder.forField(education)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле образования")
        .bind(Coach::getEducation, Coach::setEducation);
    binder.forField(workHistory)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле истории работы")
        .bind(Coach::getWorkHistory, Coach::setWorkHistory);
    binder.forField(aboutMe)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле обо мне")
        .bind(Coach::getAboutMe, Coach::setAboutMe);
    binder.forField(previewMainText)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле основного текста превью")
        .bind(Coach::getPreviewMainText, Coach::setPreviewMainText);
    binder.forField(previewSmallText)
        .withValidator(Validator.getEmptyStringValidator(), "Пустое поле короткого текста превью")
        .bind(Coach::getPreviewSmallText, Coach::setPreviewSmallText);
    // TODO make CustomUpload implement HasValue
    // binder.forField(mainPhoto)
    // .withValidator(Validator.getBytesValidator(), "Пустое поле главного фото")
    // .bind(Coach::getMainPhoto, Coach::setMainPhoto);
    // binder.forField(secondPhoto)
    // .withValidator(Validator.getBytesValidator(), "Пустое поле второго фото")
    // .bind(Coach::getSecondPhoto, Coach::setSecondPhoto);
    // binder.forField(previewPhoto)
    // .withValidator(Validator.getBytesValidator(), "Пустое поле превью фото")
    // .bind(Coach::getPreviewPhoto, Coach::setPreviewPhoto);
  }

}
