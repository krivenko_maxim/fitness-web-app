package fitness.ui.views.authentication;

import fitness.data.entity.User;
import fitness.ui.mvp.View;

public interface AuthenticationView extends View {
    void clearForms(User user);
}
