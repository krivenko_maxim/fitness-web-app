package fitness.ui.views.authentication.authorization;

import javax.servlet.http.Cookie;

import fitness.ui.views.authentication.AuthenticationView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.UserService;
import fitness.ui.mvp.Presenter;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class AuthorizationPresenter extends Presenter<AuthenticationView> {
  private final UserService userService;


  @Autowired
  public AuthorizationPresenter(UserService userService) {
    this.userService = userService;
  }


  public Cookie authorize(String email, String password) {
    return userService.authorize(email, password);
  }
}
