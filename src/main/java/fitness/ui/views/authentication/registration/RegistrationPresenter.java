package fitness.ui.views.authentication.registration;

import fitness.data.entity.User;
import fitness.ui.views.authentication.AuthenticationView;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.UserService;
import fitness.ui.mvp.Presenter;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class RegistrationPresenter extends Presenter<AuthenticationView> {
  private final UserService userService;

  @Getter
  private User userEntity = new User();

  @Autowired
  public RegistrationPresenter(UserService userService) {
    this.userService = userService;
  }

  public void save() {
    userService.register(userEntity);
  }
}
