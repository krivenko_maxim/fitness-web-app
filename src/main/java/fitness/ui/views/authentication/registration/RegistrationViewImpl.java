package fitness.ui.views.authentication.registration;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import fitness.data.entity.User;
import fitness.data.exceptions.EmailAlreadyUsedException;
import fitness.ui.views.authentication.AuthenticationViewImpl;
import fitness.ui.views.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route("registration")
@StyleSheet("./fonts.css")
@StyleSheet("./authentication/authentication.css")
public class RegistrationViewImpl extends AuthenticationViewImpl {

  private final VerticalLayout form = new VerticalLayout();
  private final RegistrationPresenter presenter;
  private PasswordField password1;
  private PasswordField password2;
  private TextField fullName;
  private EmailField email;
  private final Binder<User> binder;

  public RegistrationViewImpl(@Autowired RegistrationPresenter presenter) {
    this.binder = new Binder<>();
    this.presenter = presenter;
    this.presenter.onAttach(this);
  }

  @PostConstruct
  @Override
  public void init() {
    setupForm();
    bind();
    super.getFormBackground().add(form);
    super.init();
  }

  @Override
  public void bind() {
    binder.setBean(presenter.getUserEntity());
    binder.forField(fullName)
        .withValidator(Validator.getEmptyStringValidator(), "Фамилия и имя не могут быть пустыми")
        .withValidator(Validator.getContainsSpaceNotAtTheEndValidator(),
            "Фамилия и имя должны быть разделены пробелом")
        .bind(User::getFio, User::setFio);

    binder.forField(password1)
        .withValidator(Validator.getEmptyStringValidator(), "Пароль не может быть пустым")
        .withValidator(Validator.getPasswordValidator(),
            "Пароль должен содержать не менее 8 символов");

    binder.forField(password2)
        .withValidator(Validator.getEmptyStringValidator(), "Пожалуйста, повторите пароль")
        .withValidator(Validator.getPasswordValidator(),
            "Пароль должен содержать не менее 8 символов")
        .bind(User::getPassword, User::setPassword);

    binder.forField(email)
        .withValidator(Validator.getEmptyStringValidator(), "Email не может быть пустым")
        .bind(User::getEmail, User::setEmail);
  }

  private void setupForm() {
    fullName = new TextField("Фамилия Имя", "Фамилия Имя");
    fullName.setWidth(390, Unit.PIXELS);
    fullName.setClassName("text");

    email = new EmailField("Email", "name@domain.com");
    email.setErrorMessage("Пожалуйста введите email");
    email.setWidth(390, Unit.PIXELS);
    email.setClassName("text");

    password1 = new PasswordField("Пароль", "Введите пароль");
    password1.setWidth(390, Unit.PIXELS);
    password1.setClassName("text");

    password2 = new PasswordField("Повторите пароль", "Введите пароль");
    password2.setWidth(390, Unit.PIXELS);
    password2.setClassName("text");

    H2 title = new H2("Регистрация");
    title.setClassName("title");

    Button registrationButton = new Button("Зарегистрироваться", event -> {
      try {
        if (password1.getValue().equals(password2.getValue())) {
          binder.writeBean(presenter.getUserEntity());
          presenter.save();
          showNotification("Регистрация прошла успешно!", NotificationVariant.LUMO_SUCCESS);
        } else {
          showNotification("Введенные пароли не совпадают", NotificationVariant.LUMO_ERROR);
        }
      } catch (EmailAlreadyUsedException e) {
        showNotification("Указанный email уже занят", NotificationVariant.LUMO_ERROR);
      } catch (ValidationException e) {
        e.printStackTrace();
      }
    });
    registrationButton.setClassName("registration-button");

    Button alreadyRegisteredButton =
        new Button("Уже есть аккаунт", event -> UI.getCurrent().navigate("authorization"));
    alreadyRegisteredButton.setClassName("registered-button");

    form.add(title, fullName, email, password1, password2, registrationButton, alreadyRegisteredButton);
    form.addClassName("registration-form");
    form.addClassName("form");
  }

  @Override
  public void clearForms(User user) {
    binder.readBean(user);
  }
}
