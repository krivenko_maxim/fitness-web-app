package fitness.ui.views.coaches;

import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Coach;
import fitness.data.services.CoachService;
import fitness.ui.mvp.Presenter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CoachPresenter extends Presenter<CoachView> {

    CoachService coachService;

    public CoachPresenter(@Autowired CoachService coachService) {
        this.coachService = coachService;
    }

    public List<Coach> getCachedCoaches() {
        return coachService.getCoaches();
    }
}