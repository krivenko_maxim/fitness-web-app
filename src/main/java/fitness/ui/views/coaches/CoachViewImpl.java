package fitness.ui.views.coaches;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import fitness.data.entity.Coach;
import fitness.ui.views.coaches.coachepage.CoachPageViewImpl;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import java.io.ByteArrayInputStream;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Route("coaches")
@Transactional(readOnly = true)
@StyleSheet("./coaches-page-styles/coaches-pages-styles.css")
@StyleSheet("./fonts.css")
public class CoachViewImpl extends VerticalLayout implements CoachView {

    private final CoachPresenter presenter;

    public CoachViewImpl(@Autowired CoachPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    @PostConstruct
    public void init() {
        addClassName("background");
        getClassNames().add("no-distance");
        add(new CustomHeader(DropDownMenuColor.GREEN));
        VerticalLayout page = new VerticalLayout();
        page.setWidth("95%");
        page.getClassNames().add("no-distance");
        setMinHeight("100%");
        List<Coach> coaches = presenter.getCachedCoaches();
        for (Coach coach : coaches) {
            Image image = new Image(new StreamResource(coach.getName() + ".jpg", () -> new ByteArrayInputStream(
                coach.getPreviewPhoto())), "Фото тренера" + coach.getName());
            image.getClassNames().add("photo");

            Div photoContainer = new Div(image);
            photoContainer.getClassNames().add("photoBox");

            Div item = new Div();
            item.getClassNames().add("white-bg");

            VerticalLayout leftSide = new VerticalLayout();
            leftSide.getStyle().remove("width");
            leftSide.getClassNames().add("left-side");

            H2 mainText = new H2(coach.getPreviewMainText());
            mainText.getStyle().set("margin-right", "auto");
            Paragraph paragraph = new Paragraph(coach.getPreviewSmallText());
            paragraph.getStyle().set("margin-right", "auto");

            Button join = new Button("Присоединиться к занятиям");
            join.getClassNames().add("green-button");

            Button getMoreInfo = new Button("Узнать больше о тренере", event -> {
                UI.getCurrent().navigate(CoachPageViewImpl.class, coach.getId().toString());
            });
            getMoreInfo.getClassNames().add("white-button");

            HorizontalLayout buttons = new HorizontalLayout(join, getMoreInfo);
            buttons.getClassNames().add("center");
            leftSide.add(mainText, paragraph, buttons);

            item.add(leftSide);
            item.add(photoContainer);
            item.getStyle().set("margin-bottom", "20px");
            item.getStyle().set("margin-top", "20px");
            page.add(item);
        }
        if (coaches.isEmpty()) {
            page.getStyle().set("align-items", "center");
            Div item = new Div();
            item.getClassNames().add("white-bg");
            H2 text = new H2("Происходит загрузка тренеров, пожалуйста, подождите");
            item.add(text);
            item.getStyle().set("margin", "150px");
            item.getStyle().set("padding", "100px");

            item.getStyle().set("width", "60%");
            page.add(item);
        }
        add(page);
        add(new CustomFooter());
    }

    @Override
    public void bind() {

    }
}
