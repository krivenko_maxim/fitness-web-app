package fitness.ui.views.coaches.coachepage;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import fitness.data.entity.Coach;
import fitness.data.entity.Specialization;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import java.io.ByteArrayInputStream;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route("coach-page")
@StyleSheet("./coaches-styles/coachesPage.css")
@StyleSheet("./fonts.css")
public class CoachPageViewImpl extends Div implements CoachPageView, HasUrlParameter<String> {

    @Autowired
    private CoachPagePresenter presenter;
    private int coachId = 1;
    private Coach coach;

    public void create() {
        setWidth("100%");
        coach = presenter.getCoachById(coachId);

        Image mainImage = new Image(new StreamResource(coach.getName() + ".jpg", () -> new ByteArrayInputStream(
            coach.getMainPhoto())), "Основное фото тренера" + coach.getName());
        Image secondImage = new Image(new StreamResource(coach.getName() + ".jpg", () -> new ByteArrayInputStream(
            coach.getSecondPhoto())), "Второе фото тренера" + coach.getName());
        mainImage.setClassName("photo");
        secondImage.setClassName("photo");
        setWidth("100%");
        VerticalLayout page = new VerticalLayout();
        page.getStyle().set("padding", "0");
        page.setWidth("100%");
        getStyle().set("background-color", "#E1DEEA");
        Div mainBlock = new Div();
        mainBlock.getStyle().set("display", "flex");
        mainBlock.getStyle().set("justify-content", "space-around");
        mainBlock.getStyle().set("align-items", "center");

        mainBlock.setWidth("100%");
        VerticalLayout textBlock = new VerticalLayout();
        VerticalLayout aboutTextBlock = new VerticalLayout();
        addTextBoxes(textBlock, aboutTextBlock);
        mainBlock.add(mainImage, textBlock);

        page.add(new CustomHeader(DropDownMenuColor.GREEN), mainBlock);
        page.setHeightFull();
        Div curvedBottom = new Div();
        curvedBottom.addClassName("purpleShape");

        Div secondPage = new Div(curvedBottom);

        secondPage.getStyle().set("padding", "0");
        secondPage.getStyle().set("background-color", "#FFFFFF");
        HorizontalLayout secondPageInfo = new HorizontalLayout();
        secondPage.add(secondPageInfo);
        secondPage.addClassName("second_page");

        secondPage.add(aboutTextBlock, secondImage);
        secondPage.getStyle().set("margin", "0px");
        Div whiteCurvedBottom = new Div();
        whiteCurvedBottom.addClassName("whiteShape");
        whiteCurvedBottom.getStyle().set("margin", "0");

        HorizontalLayout lastPage = new HorizontalLayout();

        lastPage.setClassName("lastPage");

        Div title = new Div();
        H2 titleText = new H2("Тренер поможет вам");
        titleText.addClassName("titleText");
        title.add(titleText);
        title.addClassName("title");

        Div column = new Div();
        column.addClassName("column");
        column.add(getSmallTextBlock("Поддерживать мотивацию",
            "Наши занятия поднимут вам самооценку и настроение. Вы поверите в свои силы!"));
        column.add(getSmallTextBlock("Обрести легкость в теле",
            "Упражнения помогают стать более стройной, женственной и поддерживать мыщцы в тонусе."));

        column.add(getSmallTextBlock("Создать красивое рельефное тело",
            "Регулярные тренировки развивают мускулатуру, укрепляют кости, вырабатывают выносливость."));
        column.add(getSmallTextBlock("Сделать тренировки безопасными",
            "Контроль тренера, учет ваших противопоказаний принесут максимальный результат от занятий."));
        lastPage.add(title, column);
        add(page, secondPage, whiteCurvedBottom, lastPage, new CustomFooter());
    }

    private VerticalLayout getSmallTextBlock(String title, String mainText) {
        H3 label = new H3(title);

        label.setClassName("label");
        Paragraph paragraph = new Paragraph(mainText);
        paragraph.setClassName("paragraphInSmallArea");
        VerticalLayout verticalLayout = new VerticalLayout(label, paragraph);
        verticalLayout.setClassName("smallTextBox");

        return verticalLayout;
    }


    private void addTextBoxes(VerticalLayout mainTextBox, VerticalLayout aboutMeTextBox) {
        H2 coachName = new H2(coach.getName());
        coachName.addClassName("titleText");
        Paragraph coachMainInfo = new Paragraph(coach.getMainInfo());

        VerticalLayout verticalLayout = new VerticalLayout(coachName, coachMainInfo);

        mainTextBox.add(verticalLayout);
        mainTextBox.setClassName("textBlock");

        H3 spec = new H3("Специализации");
        spec.setClassName("label");

        verticalLayout = new VerticalLayout(spec);

        UnorderedList unorderedList = new UnorderedList();
        for (Specialization specialization : coach.getSpecializations()) {
            ListItem item = new ListItem(specialization.getInfo());
            unorderedList.add(item);
        }
        verticalLayout.add(unorderedList);
        mainTextBox.add(verticalLayout);
        H3 education = new H3("Образование");
        education.setClassName("label");
        Paragraph educationInfo = new Paragraph(coach.getEducation());
        verticalLayout = new VerticalLayout(education, educationInfo);

        mainTextBox.add(verticalLayout);

        verticalLayout = new VerticalLayout();

        mainTextBox.add(verticalLayout);
        H3 title = new H3("О себе");
        title.addClassNames("label", "left-align");

        aboutMeTextBox.add(title);

        for (String s : coach.getAboutMe().split("\n\n")) {
            aboutMeTextBox.add(new Paragraph(s));
        }

        Button join = new Button("Записаться");
        join.getClassNames().add("green-button");

        aboutMeTextBox.setClassName("textBlock");
        aboutMeTextBox.add(join);
    }

    @PostConstruct
    @Override
    public void init() {

    }

    @Override
    public void bind() {

    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        coachId = Integer.parseInt(s);
        create();
    }
}
