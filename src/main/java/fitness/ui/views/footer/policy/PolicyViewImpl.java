package fitness.ui.views.footer.policy;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;

@Route("policy")
@StyleSheet("./fonts.css")
@StyleSheet("./policy-terms-styles/policy-terms.css")
public class PolicyViewImpl extends VerticalLayout implements PolicyView {

    @Override
    @PostConstruct
    public void init() {
        setClassName("background");

        Div policyDiv = new Div();
        String text = "Политика конфиденциальности не доступна";
        try {
            String fileName = "textfiles/policy.txt";
            text = StreamUtils.copyToString(new ClassPathResource(fileName).getInputStream(),
                Charset.defaultCharset());

        } catch (IOException e) {
            e.printStackTrace();
        }
        policyDiv.getElement().setProperty("innerHTML", text);
        policyDiv.getStyle().set("padding-bottom", "3%");

        Component[] components = {new CustomHeader(DropDownMenuColor.GREEN), policyDiv, new CustomFooter()};
        add(components);
    }


    @Override
    public void bind() {

    }
}
