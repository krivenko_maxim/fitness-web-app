package fitness.ui.views.footer.usermanual;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;

@Route("usermanual")
@StyleSheet("./fonts.css")
@StyleSheet("./policy-terms-styles/policy-terms.css")
public class UserManualViewImpl extends VerticalLayout implements UserManualView {

    @Override
    @PostConstruct
    public void init() {
        setClassName("background");

        String text = "Политика конфиденциальности не доступна";
        try {
            String FILE_NAME = "textfiles/agreement.txt";
            text = StreamUtils.copyToString( new ClassPathResource(FILE_NAME).getInputStream(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Div userManualDiv = new Div();
        userManualDiv.getElement().setProperty("innerHTML", text);
        userManualDiv.getStyle().set("padding-bottom", "3%");

        Component[] components = {new CustomHeader(DropDownMenuColor.GREEN), userManualDiv, new CustomFooter()};
        add(components);
    }

    @Override
    public void bind() {

    }
}
