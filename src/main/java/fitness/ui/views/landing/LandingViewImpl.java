package fitness.ui.views.landing;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import fitness.ui.views.utils.NoThrowFileInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Route("landing")
@CssImport("./style.css")
public class LandingViewImpl extends Div implements LandingView {

    public LandingViewImpl() {

    }

    @Override
    @PostConstruct
    public void init() {
        Div headerWrapper = new Div(new CustomHeader(DropDownMenuColor.WHITE));
        headerWrapper.getClassNames().add("landing");
        headerWrapper.getStyle().set("background",
            "linear-gradient(270.31deg, #C90452 0.33%, rgba(115, 90, 154, 0.67) 49.02%, #00CCFA 99.79%");
        add(headerWrapper);
        add(new Html(
            NoThrowFileInputStream.construct("src/main/resources/META-INF/resources/frontend/homepage/index.html")));
        add(new CustomFooter());
    }

    @Override
    public void bind() {

    }
}
