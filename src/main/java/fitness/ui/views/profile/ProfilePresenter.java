package fitness.ui.views.profile;

import fitness.data.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.UserService;
import fitness.ui.mvp.Presenter;
import javax.servlet.http.Cookie;
import lombok.Getter;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ProfilePresenter extends Presenter<ProfileView> {

    private final UserService userService;

    @Getter
    private User userEntity = new User();

    @Autowired
    public ProfilePresenter(UserService userService) {
        this.userService = userService;
    }

    public User getUserFromCookies(Cookie[] cookies) {
        return userService.getUserFromCookies(cookies);
    }

    public void updateUser() {
        userService.updatePersonalData(userEntity);
    }
}
