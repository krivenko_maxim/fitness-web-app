package fitness.ui.views.profile;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinRequest;
import fitness.data.entity.User;
import fitness.data.exceptions.JwtNotFoundException;
import fitness.data.exceptions.UserNotFoundException;
import fitness.ui.vaadin.components.CustomUpload;
import fitness.ui.views.utils.Converter;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import fitness.ui.views.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;

@Route("profile")
@StyleSheet("./fonts.css")
@StyleSheet("./profile-styles/profile.css")
public class ProfileViewImpl extends VerticalLayout implements ProfileView {

  private final ProfilePresenter presenter;
  private User user;
  private PasswordField password;
  private TextField userFirstName;
  private TextField userLastName;
  private EmailField email;
  private Button cancel;
  private Button save;
  private CustomUpload avatarUpload;
  private Binder<User> binder;

  public ProfileViewImpl(@Autowired ProfilePresenter presenter) {
    this.binder = new Binder<>();
    this.presenter = presenter;
    this.presenter.onAttach(this);
  }

  @Override
  @PostConstruct
  public void init() {

  }

  public void create() {
    setClassName("background");
    Div line = new Div();
    line.setClassName("line");

    Div whiteFrame = new Div();
    whiteFrame.setClassName("white_frame");

    Div userFrame = setupDataFrame();
    Div changeFrame = setupChangeFrame();
    Div buttons = setupButtons();
    whiteFrame.add(userFrame, line, changeFrame, buttons);

    Component[] components = {new CustomHeader(DropDownMenuColor.GREEN), whiteFrame, new CustomFooter()};
    add(components);
  }

  private Div setupDataFrame() {
    Div userDataFrame = new Div();
    userDataFrame.getClassNames().add("user-form");

    Image avatar;
    StreamResource avatarResource =
        new StreamResource("avatar.png", () -> new ByteArrayInputStream(user.getPhoto()));
    avatar = new Image(avatarResource, "a/photo pic");
    avatar.setClassName("avatar");

    H2 userName = new H2(user.getFirstName() + " " + user.getLastName());
    userName.setClassName("fio");

    avatarUpload = new CustomUpload();
    avatarUpload.setClassName("avatar-upload");
    Button uploadButton = new Button();
    uploadButton.setIcon(new Icon(VaadinIcon.UPLOAD_ALT));
    Icon emptyIcon = new Icon();
    emptyIcon.setColor("#FFFFFF");
    avatarUpload.setUploadButton(uploadButton);
    avatarUpload.setDropLabelIcon(emptyIcon);
    avatarUpload.setDropLabel(new Label("Обновить фото"));
    avatarUpload.addFinishedListener(event -> {
      StreamResource newAvatarResource = new StreamResource("avatar.png",
          () -> new ByteArrayInputStream(avatarUpload.getUploadedFile()));
      avatar.setSrc(newAvatarResource);
      avatarUpload.getElement().executeJs("this.files=[]");
      save.setEnabled(true);
    });

    H3 userEmail = new H3(user.getEmail());
    userEmail.setClassName("email");
    Div avatarBlock = new Div(avatar, avatarUpload);
    Div textBlock = new Div(userName, userEmail);
    textBlock.getClassNames().add("text-block");
    userDataFrame.add(avatarBlock, textBlock);
    return userDataFrame;
  }

  private Div setupChangeFrame() {
    Div updateDataFrame = new Div();

    userLastName = new TextField("Фамилия", "Введите новую фамилию");
    userLastName.getClassNames().add("field");
    userLastName.addValueChangeListener(event -> setSaveStateFromFieldsEmptiness());

    userFirstName = new TextField("Имя", "Введите новое имя");
    userFirstName.getClassNames().add("field");
    userFirstName.addValueChangeListener(event -> setSaveStateFromFieldsEmptiness());

    email = new EmailField("Почта", "Введите новую почту");
    email.getClassNames().add("field");
    email.addValueChangeListener(event -> setSaveStateFromFieldsEmptiness());

    password = new PasswordField("Пароль", "Введите новый пароль");
    password.getClassNames().add("field");
    password.addValueChangeListener(event -> setSaveStateFromFieldsEmptiness());

    updateDataFrame.add(userLastName, userFirstName, email, password);
    updateDataFrame.setClassName("centre");
    return updateDataFrame;
  }

  private Div setupButtons() {
    cancel = new Button("Отменить");
    cancel.setClassName("button-cancel");
    cancel.addClickListener(event -> {
      userFirstName.clear();
      userLastName.clear();
      password.clear();
      email.clear();
      save.setEnabled(false);
    });
    save = new Button("Сохранить");
    save.setClassName("button-save");
    save.setEnabled(false);
    save.addClickListener(event -> {
      try {
        binder.writeBean(presenter.getUserEntity());
        if (!avatarUpload.isEmpty()) {
          presenter.getUserEntity().setPhoto(avatarUpload.getUploadedFile());
        } else {
          presenter.getUserEntity().setPhoto(user.getPhoto());
        }
        presenter.updateUser();
        showNotification("Успешно загружено", NotificationVariant.LUMO_SUCCESS);
      } catch (ValidationException e) {
        e.printStackTrace();
      }
    });

    Div buttonFrame = new Div();
    buttonFrame.setClassName("button-frame");
    buttonFrame.add(cancel, save);
    return buttonFrame;
  }

  private void setSaveStateFromFieldsEmptiness() {
    save.setEnabled(!userFirstName.isEmpty() || !userLastName.isEmpty() || !email.isEmpty()
        || !password.isEmpty() || !avatarUpload.isEmpty());
  }

  @Override
  public void bind() {
    binder.setBean(presenter.getUserEntity());
    binder.forField(userFirstName).withNullRepresentation("")
        .withConverter(Converter.getConverterInvokeGetterIfNullOrEmpty(User::getFirstName, user),
            storedFirstName -> storedFirstName)
        .bind(User::getFirstName, User::setFirstName);

    binder.forField(userLastName).withNullRepresentation("")
        .withConverter(Converter.getConverterInvokeGetterIfNullOrEmpty(User::getLastName, user),
            storedLastName -> storedLastName)
        .bind(User::getLastName, User::setLastName);

    binder.forField(password).withNullRepresentation("")
        .withConverter(Converter.getConverterInvokeGetterIfNullOrEmpty(User::getPassword, user),
            storedPassword -> storedPassword)
        .withValidator(Validator.getPasswordValidator(),
            "Пароль должен содержать не менее 8 символов")
        .bind(User::getPassword, User::setPassword);

    binder.forField(email).withNullRepresentation("")
        .withConverter(Converter.getConverterInvokeGetterIfNullOrEmpty(User::getEmail, user),
            storedEmail -> storedEmail)
        .bind(User::getEmail, User::setEmail);
    // TODO bind custom upload
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {
    try {
      user = presenter.getUserFromCookies(VaadinRequest.getCurrent().getCookies());
    } catch (JwtNotFoundException | UserNotFoundException e) {
      e.printStackTrace();
      // TODO this will be done by security
      UI.getCurrent().navigate("authorization");
      return;
    }
    presenter.getUserEntity().setId(user.getId());
    create();
    bind();
  }

}
