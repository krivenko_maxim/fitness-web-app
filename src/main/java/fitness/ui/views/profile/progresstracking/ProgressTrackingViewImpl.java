package fitness.ui.views.profile.progresstracking;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import fitness.data.entity.Weight;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.vaadin.components.canvas.Canvas;
import fitness.ui.views.utils.DropDownMenuColor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("progress-tracking")
@JsModule("https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js")
@JavaScript("./js/drawChart.js")
@CssImport("./progress-tracking-styles/progress-tracking.css")
public class ProgressTrackingViewImpl extends VerticalLayout implements ProgressTrackingView
{
    private final ProgressTrackingPresenter presenter;

    private List<Weight> weights;

    public ProgressTrackingViewImpl(@Autowired ProgressTrackingPresenter presenter)
    {
        this.presenter = presenter;
        presenter.onAttach(this);
    }

    @PostConstruct
    @Override
    public void init()
    {
        setWidth("100%");
        setClassName("no-distance");

        VerticalLayout weightLayout = createWeightLayout();
        Component[] components = {new CustomHeader(DropDownMenuColor.WHITE), weightLayout, new CustomFooter()};
        add(components);
    }

    @Override
    public void bind()
    {

    }

    private VerticalLayout createWeightLayout()
    {
        VerticalLayout weightLayout = new VerticalLayout();
        weightLayout.setClassName("weight-layout");
        weightLayout.setWidth("100%");

        HorizontalLayout firstLayout = new HorizontalLayout();
        firstLayout.setClassName("first-layout");
        firstLayout.setWidth("100%");

        VerticalLayout initialWeight = createInitialWeight();
        VerticalLayout currentWeight = createCurrentWeight();
        VerticalLayout requiredWeight = createRequiredWeight();

        firstLayout.add(initialWeight, currentWeight, requiredWeight);

        Div chartContainer = new Div();
        Canvas canvas = new Canvas(1000, 400);
        canvas.setWidth("100%");
        canvas.setId("chart-canvas");
        chartContainer.add(canvas);
        chartContainer.setClassName("chart-container");

        HorizontalLayout secondLayout = new HorizontalLayout();
        secondLayout.setClassName("second-layout");
        secondLayout.setWidth("100%");

        VerticalLayout leftWeight = createLeftWeight();
        VerticalLayout changes = createChanges();

        secondLayout.add(leftWeight, changes);

        weightLayout.add(firstLayout, chartContainer, secondLayout);
        return weightLayout;
    }

    private VerticalLayout createInitialWeight()
    {
        VerticalLayout layout = new VerticalLayout();
        H4 initialWeight = new H4(String.valueOf(presenter.getInitialWeight()));
        initialWeight.addClassName("num-initial-weight");
        H5 initialWeightLabel = new H5("Initial weight, kg");
        initialWeightLabel.addClassName("label-initial-weight");
        layout.add(initialWeight, initialWeightLabel);
        return layout;
    }

    private VerticalLayout createCurrentWeight()
    {
        VerticalLayout layout = new VerticalLayout();
        H4 currentWeight = new H4(presenter.getCurrentWeight());
        currentWeight.addClassName("num-current-weight");
        H5 currentWeightLabel = new H5("Current weight, kg");
        currentWeightLabel.addClassName("label-current-weight");
        layout.add(currentWeight, currentWeightLabel);
        return layout;
    }

    private VerticalLayout createRequiredWeight()
    {
        VerticalLayout layout = new VerticalLayout();
        H4 requiredWeight = new H4(String.valueOf(presenter.getRequiredWeight()));
        requiredWeight.addClassName("num-required-weight");
        H5 requiredWeightLabel = new H5("Required weight, kg");
        requiredWeightLabel.addClassName("label-required-weight");
        layout.add(requiredWeight, requiredWeightLabel);
        return layout;
    }

    private VerticalLayout createLeftWeight()
    {
        VerticalLayout layout = new VerticalLayout();
        H4 leftWeight = new H4(presenter.getLeftWeight());
        leftWeight.addClassName("num-left-weight");
        H5 leftWeightLabel = new H5("Left weight, kg");
        leftWeightLabel.addClassName("label-left-weight");
        layout.add(leftWeight, leftWeightLabel);
        return layout;
    }

    private VerticalLayout createChanges()
    {
        VerticalLayout layout = new VerticalLayout();
        H4 changes = new H4(presenter.getChanges());
        changes.addClassName("num-changes");
        H5 changesLabel = new H5("Changes, kg");
        changes.addClassName("label-changes");
        layout.add(changes, changesLabel);
        return layout;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent)
    {
        /*String weights = getWeights();
        String dates = getDates();*/

        //placeholder until proper initialWeight implementation

        String weights = "50, 60, 70, 80, 90, 100";
        String dates = "14.02, 15.02, 16.02, 17.02, 18.02, 19.02";
        String initialWeight = "40";
        String requiredWeight = "120";

        UI.getCurrent().getPage().executeJs("run($0, $1, $2, $3, $4)",
                weights, dates, initialWeight, requiredWeight, 6);
    }
}
