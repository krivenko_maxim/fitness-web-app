package fitness.ui.views.profile.videoplayer;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.server.AbstractStreamResource;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Tag("video")
public class VideoContainer extends HtmlContainer implements ClickNotifier<Image>
{
  private static final PropertyDescriptor<String, String> SRC_DESCRIPTOR = PropertyDescriptors
          .attributeWithDefault("src", "");

  public VideoContainer()
  {
    super();
    this.getElement().setProperty("controls", false);
  }

  public VideoContainer(String src)
  {
    this();
    this.setSrc(src);
  }

  public String getSrc()
  {
    return get(SRC_DESCRIPTOR);
  }

  public void setSrc(String src)
  {
    set(SRC_DESCRIPTOR, src);
  }

  public void setSrc(AbstractStreamResource src)
  {
    getElement().setAttribute("src", src);
  }

  public void setVolume(double volume) {
    this.getElement().setProperty("volume", volume);
  }
}
