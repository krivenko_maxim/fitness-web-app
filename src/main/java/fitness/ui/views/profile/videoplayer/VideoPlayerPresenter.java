package fitness.ui.views.profile.videoplayer;

import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Video;
import fitness.data.exceptions.JwtNotFoundException;
import fitness.data.exceptions.VideoNotFoundException;
import fitness.data.services.UserService;
import fitness.data.services.VideoService;
import fitness.ui.mvp.Presenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class VideoPlayerPresenter extends Presenter<VideoPlayerView> {

  private final VideoService videoService;
  private final UserService userService;

  private int currentVideoIndex = 0;
  private List<Video> videos;
  
  public VideoPlayerPresenter(@Autowired VideoService videoService,
                              @Autowired UserService userService) {
    this.videoService = videoService;
    this.userService = userService;
    //TODO get videos for current user
  }

  public StreamResource getStreamResourceOfVideo(String path) {
    return videoService.getStreamResourceOfVideo(path);
  }

  public List<Video> getVideos() {
    return videoService.getVideos();
  }

  public List<Video> getVideosForCurrentUser() {
    return videoService.
            getVideosByUser(userService.
                    getUserFromCookies(VaadinService.
                            getCurrentRequest().getCookies()));
  }

  public void updateVideoList() {
    //TODO get videos for current user
    videos = getVideos();
  }

  public StreamResource getCurrentVideo() {
    return getStreamResourceOfVideo(videos.get(currentVideoIndex).getVideoPath());
  }

  public void moveCursor(Step step)
  {
    if (currentVideoIndex >= videos.size() - 1 && step == Step.FORWARD) {
      currentVideoIndex = 0;
    } else if (currentVideoIndex <= 0 && step == Step.BACKWARDS) {
      currentVideoIndex = videos.size() - 1;
    } else {
      currentVideoIndex += step.getStep();
    }
  }

  public enum Step {
    FORWARD(1),
    BACKWARDS(-1);

    private final int step;

    Step(int step) {
      this.step = step;
    }

    public int getStep() {
      return step;
    }
  }
}
