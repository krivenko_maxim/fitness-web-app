package fitness.ui.views.profile.videoplayer;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Input;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.theme.lumo.Lumo;
import fitness.data.exceptions.JwtNotFoundException;
import fitness.data.exceptions.VideoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;


import static fitness.ui.views.profile.videoplayer.VideoPlayerPresenter.Step;

@Route(value = "videos/")
@CssImport("./videoplayer-styles/video-player.css")
public class VideoPlayerViewImpl extends VerticalLayout implements VideoPlayerView
{
  private final VideoPlayerPresenter videoPlayerPresenter;
  
  private final VideoContainer video = new VideoContainer();
  
  private final VerticalLayout form = new VerticalLayout();
  
  private final Div videoPanel = new Div();
  private final Div videoContainer = new Div();
  private final Div videoControls = new Div();

  private final Icon playIcon = new Icon(VaadinIcon.PLAY);
  private final Icon stopIcon = new Icon(VaadinIcon.STOP);

  private final Button nextButton = new Button("Следующее видео");
  private final Button prevButton = new Button("Предыдущее видео");
  private final Button playButton = new Button(playIcon);
  private final Input volumeSlider = new Input();

  public VideoPlayerViewImpl(@Autowired VideoPlayerPresenter videoPlayerPresenter) {
    this.videoPlayerPresenter = videoPlayerPresenter;
    this.videoPlayerPresenter.onAttach(this);

    try {
      updateVideoList();
    }
    catch (VideoNotFoundException e) {
      e.printStackTrace();
      UI.getCurrent().navigate("buy");
      return;
    }
    catch (JwtNotFoundException e) {
      UI.getCurrent().navigate("authorization");
      return;
    }

    try {
      video.setSrc(videoPlayerPresenter.getCurrentVideo());
    }
    catch (VideoNotFoundException e) {
      //TODO forward user to the error page
      e.printStackTrace();
    }

    setClassNames();
    setSliderAttributes();
    setupButtons();
    fillDivBlocks();
  }


  public void setClassNames() {
    videoPanel.setClassName("video-panel");
    form.addClassName("videoplayer-form");
    videoContainer.setClassName("video-container");

    videoControls.setClassName("video-controls");
    prevButton.addClassName("prev-button");
    nextButton.addClassName("next-button");
    playButton.addClassName("play-button");
    volumeSlider.addClassName("volume-slider");
    volumeSlider.getElement().setAttribute("theme", Lumo.DARK);

    video.setClassName("video-player");
  }


  public void setSliderAttributes() {
    volumeSlider.getElement().setAttribute("type", "range");
    volumeSlider.getElement().setAttribute("min", "1");
    volumeSlider.getElement().setAttribute("max", "100");
    volumeSlider.getElement().setAttribute("value", "50");
    volumeSlider.getElement().setAttribute("id", "volume");
  }
  
  private void setupButtons() {
    nextButton.addClickListener(e -> onVideoSwitch(Step.FORWARD, video));
    nextButton.addClickShortcut(Key.ARROW_RIGHT);
    prevButton.addClickListener(e -> onVideoSwitch(Step.BACKWARDS, video));
    prevButton.addClickShortcut(Key.ARROW_LEFT);
    playButton.addClickListener(e -> onClickPlay());
    playButton.addClickShortcut(Key.SPACE);
    volumeSlider.addValueChangeListener(e ->
            video.setVolume(Double.parseDouble(volumeSlider.getValue()) * 0.01));
  }
  
  private void fillDivBlocks() {
    video.getElement().setProperty("paused", true);
    videoControls.add(prevButton, playButton, volumeSlider, nextButton);
    add(video);
    videoPanel.add(videoControls);
    add(videoPanel);
  }
  
  private void onVideoSwitch(Step step, VideoContainer video) {
    videoPlayerPresenter.moveCursor(step);
    video.setSrc(videoPlayerPresenter.getCurrentVideo());
  }
  
  private void onClickPlay() {
    if (!Boolean.parseBoolean(video.getElement().getProperty("paused"))) {
      video.getElement().callJsFunction("pause");
      video.getElement().setProperty("paused", true);
      playButton.setIcon(playIcon);
    }
    else {
      video.getElement().callJsFunction("play");
      video.getElement().setProperty("paused", false);
      playButton.setIcon(stopIcon);
    }
  }

  private void updateVideoList() {
    //TODO get videos for current user (security is not yet enabled),
    // method videoPlayerPresenter.getVideosForCurrentUser()
    videoPlayerPresenter.updateVideoList();
  }

  @Override
  public void init() {

  }

  @Override
  public void bind() {
  }
}
