package fitness.ui.views.utils;

public enum DropDownMenuColor
{
  GREEN("#98F357"),
  WHITE("#FFFFFF");
  
  private String colorRepresentation;
  
  DropDownMenuColor(String colorRepresentation)
  {
    this.colorRepresentation = colorRepresentation;
  }
  
  public String getColorRepresentation()
  {
    return colorRepresentation;
  }
}
