package fitness.ui.views.utils;

import fitness.data.exceptions.HtmlFileNotFoundException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class NoThrowFileInputStream {

    public static FileInputStream construct(String name) {
        try {
            return new FileInputStream(name);
        } catch (FileNotFoundException e) {
            throw new HtmlFileNotFoundException(e.getMessage(), e);
        }

    }
}
