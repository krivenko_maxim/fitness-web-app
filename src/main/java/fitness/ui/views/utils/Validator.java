package fitness.ui.views.utils;

import com.vaadin.flow.function.SerializablePredicate;

public class Validator {
  public static SerializablePredicate<String> getContainsSpaceNotAtTheEndValidator() {
    return input -> (input.indexOf(' ') > 0) && (input.indexOf(' ') < input.length() - 1);
  }

  public static SerializablePredicate<String> getEmptyStringValidator() {
    return input -> input != null && !input.isEmpty();
  }

  public static SerializablePredicate<String> getPasswordValidator() {
    return input -> String.valueOf(input).length() >= 8;
  }
}
