package fitness.ui.views.workouts;

import com.vaadin.flow.router.NotFoundException;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Tariff;
import fitness.data.entity.TariffDescription;
import fitness.data.entity.Workout;
import fitness.data.services.TariffDescriptionService;
import fitness.data.services.TariffService;
import fitness.data.services.WorkoutService;
import fitness.ui.mvp.Presenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class WorkoutPresenter extends Presenter<WorkoutView> {

    private final TariffService tariffService;
    private final WorkoutService workoutService;
    private final TariffDescriptionService tariffDescriptionService;

    public WorkoutPresenter(@Autowired TariffService tariffService, @Autowired WorkoutService workoutService,
        @Autowired TariffDescriptionService tariffDescriptionService) {
        this.tariffService = tariffService;
        this.workoutService = workoutService;
        this.tariffDescriptionService = tariffDescriptionService;
    }

    public Tariff updateTariff(Tariff entity) {
        return tariffService.update(entity);
    }

    public void updateDescriptions(List<TariffDescription> tariffDescriptions) {
        for (TariffDescription tariffDescription : tariffDescriptions) {
            tariffDescriptionService.update(tariffDescription);
        }
    }

    public List<Tariff> getTariffs() {
        return tariffService.findAll();
    }

    public Tariff getTariff(String name) {
        return tariffService.getTariffByName(name).orElseThrow(NotFoundException::new);
    }

    public List<String> getWorkouts(int number) {
        Workout workout = workoutService.get(number).orElseThrow(NotFoundException::new);
        return List.of(workout.getProgram_1(), workout.getProgram_2(), workout.getProgram_3());
    }

    public List<TariffDescription> getDescriptionsByTariffName(String name) {
        return tariffDescriptionService.getDescriptionsByTariffName(name);
    }
}