window.run = function run(dataString, labelsString, initialWeightString, requiredWeightString, amount)
{
    const data = {
        labels: labelsString.split(","),
        datasets: [{
            label: 'Вес',
            data: dataString.split(","),
            fill: false,
            borderColor: 'rgb(160, 145, 192)',
            tension: 0.1
        },
        {
            label: 'Начальный вес',
            data: Array(amount).fill(initialWeightString),
            fill: true,
            borderColor: 'rgb(227, 194, 215)',
            tension: 0.1
        },
        {
            label: 'Желаемый вес',
            data: Array(amount).fill(requiredWeightString),
            fill: true,
            borderColor: 'rgb(152, 206, 195)',
            tension: 0.1
        }]
    };

    const ctx = document.getElementById('chart-canvas').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: data,
    });

}


