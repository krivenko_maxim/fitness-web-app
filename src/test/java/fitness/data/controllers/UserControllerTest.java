package fitness.data.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import fitness.data.entity.User;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.Cookie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import fitness.EmptySecurityConfig;
import fitness.data.controllers.handlers.UserExceptionHandler;
import fitness.data.exceptions.EmailAlreadyUsedException;
import fitness.data.exceptions.InvalidCredentialsException;
import fitness.data.exceptions.UserNotFoundException;
import fitness.data.services.UserService;

@WebMvcTest(controllers = UserController.class,
    excludeAutoConfiguration = SecurityAutoConfiguration.class)
@ContextConfiguration(
    classes = {EmptySecurityConfig.class, UserController.class, UserExceptionHandler.class})
public class UserControllerTest {

  private static final String CONTROLLER_MAPPING = "/api/users";
  private static final int COMMON_ID1 = 1;
  private static final int COMMON_ID2 = 2;
  private static final String COMMON_EMAIL = "em@ail.dot";
  private static final String COMMON_PASSWORD = "12345678";
  private static final Cookie COMMON_COOKIE = new Cookie("jwt", "qweqweqweqe");
  private User user1;
  private User user2;

  @Autowired
  private MockMvc mvc;

  @MockBean
  private UserService service;

  @BeforeEach
  public void setup() {
    user1 = new User();
    user1.setId(COMMON_ID1);
    user1.setEmail(COMMON_EMAIL);
    user1.setPassword(COMMON_PASSWORD);
    user1.setRoles("ROLE_USER");
    user2 = new User();
    user2.setId(COMMON_ID2);
    user2.setEmail(COMMON_EMAIL);
    user2.setRoles("ROLE_USER");
    user2.setPassword(COMMON_PASSWORD);
  }

  @Test
  public void findAllWhenNoneExist() throws Exception {
    when(service.findAll()).thenReturn(Collections.emptyList());
    mvc.perform(get(CONTROLLER_MAPPING)).andExpect(status().isOk()).andExpect(content().json("[]"));
  }

  @Test
  public void findAllWhenMultipleExist() throws Exception {
    List<User> list = List.of(user1, user2);
    when(service.findAll()).thenReturn(list);

    mvc.perform(get(CONTROLLER_MAPPING)).andExpect(status().isOk())
        .andExpect(content().json(new ObjectMapper().writeValueAsString(list)));
  }

  @Test
  public void findByIdWhenNoneExist() throws Exception {
    when(service.findById(COMMON_ID1)).thenThrow(UserNotFoundException.class);
    mvc.perform(get(CONTROLLER_MAPPING + "/ids/" + COMMON_ID1)).andExpect(status().isNotFound())
        .andExpect(content().string(""));
  }

  @Test
  public void findByIdWhenIdExists() throws Exception {
    when(service.findById(COMMON_ID1)).thenReturn(user1);
    mvc.perform(get(CONTROLLER_MAPPING + "/ids/" + COMMON_ID1)).andExpect(status().isOk())
        .andExpect(content().json(new ObjectMapper().writeValueAsString(user1)));
  }

  @Test
  public void findByEmailWhenNoneExist() throws Exception {
    doThrow(UserNotFoundException.class).when(service).findByEmail(COMMON_EMAIL);
    mvc.perform(get(CONTROLLER_MAPPING + "/emails/" + COMMON_EMAIL))
        .andExpect(status().isNotFound()).andExpect(content().string(""));
  }

  @Test
  public void findByEmailWhenEmailExists() throws Exception {
    when(service.findByEmail(COMMON_EMAIL)).thenReturn(user1);
    mvc.perform(get(CONTROLLER_MAPPING + "/emails/" + COMMON_EMAIL)).andExpect(status().isOk())
        .andExpect(content().json(new ObjectMapper().writeValueAsString(user1)));
  }

  @Test
  public void authorizeWhenUserNotFound() throws Exception {
    doThrow(UserNotFoundException.class).when(service).authorize(COMMON_EMAIL, COMMON_PASSWORD);
    mvc.perform(post(CONTROLLER_MAPPING + "/authorize").param("email", COMMON_EMAIL)
        .param("password", COMMON_PASSWORD)).andExpect(status().isNotFound())
        .andExpect(content().string(""));
  }

  @Test
  public void authorizeWhenInvalidCredentials() throws Exception {
    doThrow(InvalidCredentialsException.class).when(service).authorize(COMMON_EMAIL,
        COMMON_PASSWORD);
    mvc.perform(post(CONTROLLER_MAPPING + "/authorize").param("email", COMMON_EMAIL)
        .param("password", COMMON_PASSWORD)).andExpect(status().isUnauthorized())
        .andExpect(content().string(""));
  }

  @Test
  public void authorizeCommonCase() throws Exception {
    when(service.authorize(COMMON_EMAIL, COMMON_PASSWORD)).thenReturn(COMMON_COOKIE);
    mvc.perform(post(CONTROLLER_MAPPING + "/authorize").param("email", COMMON_EMAIL)
        .param("password", COMMON_PASSWORD)).andExpect(status().isOk())
        .andExpect(content().string(""))
        .andExpect(cookie().value(COMMON_COOKIE.getName(), COMMON_COOKIE.getValue()));
  }

  @Test
  public void registerWhenEmailAlreadyUsed() throws Exception {
    doThrow(EmailAlreadyUsedException.class).when(service).register(any());
    mvc.perform(post(CONTROLLER_MAPPING + "/register").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(user1))).andExpect(status().isConflict())
        .andExpect(content().string(""));
  }

  @Test
  public void registerCommonCase() throws Exception {
    when(service.register(any())).thenReturn(user1);
    mvc.perform(post(CONTROLLER_MAPPING + "/register").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(user1))).andExpect(status().isOk())
        .andExpect(content().string(""));
  }


  @Test
  public void updateWhenUserNotFound() throws Exception {
    doThrow(UserNotFoundException.class).when(service).update(any());
    mvc.perform(put(CONTROLLER_MAPPING).contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(user1))).andExpect(status().isNotFound())
        .andExpect(content().string(""));
  }

  @Test
  public void updateCommonCase() throws Exception {
    when(service.update(any())).thenReturn(user1);
    mvc.perform(put(CONTROLLER_MAPPING).contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(user1))).andExpect(status().isOk())
        .andExpect(content().string(""));
  }

  @Test
  public void deleteAllCommonCase() throws Exception {
    doNothing().when(service).deleteAll();
    mvc.perform(delete(CONTROLLER_MAPPING)).andExpect(status().isOk())
        .andExpect(content().string(""));
  }

  @Test
  public void deleteByIdCommonCase() throws Exception {
    doNothing().when(service).deleteById(COMMON_ID1);
    mvc.perform(delete(CONTROLLER_MAPPING + "/ids/" + COMMON_ID1)).andExpect(status().isOk())
        .andExpect(content().string(""));
  }

  @Test
  public void deleteByEmailCommonCase() throws Exception {
    doNothing().when(service).deleteByEmail(COMMON_EMAIL);
    mvc.perform(delete(CONTROLLER_MAPPING + "/emails/" + COMMON_EMAIL)).andExpect(status().isOk())
        .andExpect(content().string(""));
  }
}
